conky-terminal
==============
>URxvt as Conky Widget

![Sample Gif](https://gitlab.com/aquator/conky-terminal/-/raw/master/sample.gif)

Features
--------
- Terminal in specified size and position
- Multi monitor support
- Relative sizing based on screen size
- Anchor points: set distances relative to screen edges, or center

Usage
-----
```
Usage: conky-terminal [-a <anchor] [-d <size>] [-h] [-o <opts>] [-p <pos>] [-s <name>] [-v] [-V] <program>
Executes a program in an urxvt terminal embedded in a transparent Conky window as background.

Arguments:
 -a <anchor>   Sets where to calculate position from. Valid values are
               top-left, top-center, top-right
               center-left, center-center, center-right
               bottom-left, bottom-center, bottom-right
               Can be shortened as: tl, tc, tr, cl, cc, cr, bl, bc, br
               Position will be calculated from anchor point of screen to anchor
               point of window. Default is center-center.
 -b <color>    Background color of terminal with true transparency.
               Default is rgba:0000/0000/0000/0000 (Transparent)
 -d <size>     Dimensions of window in pixels, in WIDTHxHEIGHT format.
               Percentages can be used as well.
               Default is 100%x100%.
 -h            Prints this help page.
 -o <opts>     Extra URxvt options to pass.
 -p <pos>      Position of terminal on pixels, in X,Y format. Negative values are allowed.
               Default is 0,0.
 -s <screen>   Screen identifier, as listed in xrandr. Falls back to primary screen.
 -v            Verbose, for debugging.
 -V            Print version information.

Example:
  # Executes cava (Console-based Audio Visualizer for ALSA)
  # as an animated background overlay in the specified screen.
$ conky-terminal -s DP-1 cava
  # Executes top in a 800x600 window with a black background.
  # keeping an 50px margin from the bottom and right edges.
$ conky-terminal -d 800x600 -p -50,-50 -abr -b black top
  # Use extra URxvt argument with spaces, like a customized font:
$ conky-terminal -o '-fn "xft:JetBrains Mono Nerd Font Mono:pixelsize=8"' neofetch
```

Installation
------------
You can just download the script file.

Requirements
------------
Created for [i3wm] in [Bash]. Uses **xrandr** for positioning, [Conky] for background window, [URxvt] or a fork for command line wrapper. Bash **sleep** extension is required for waiting Conky window to appear (but it can be removed). Tools like **sed**, **grep** and **cat** are also used.

Hints
-----
#### Conky windows are blurred/having shadows in Picom
Since I created this for [i3wm] in the first place, the Conky window must be "override" type so the WM ignores it. That way class based rules will not work in Picom, but you can do this instead, to disable effects on Unknown windows:
```
blur-background-exclude = [
     "! name~=''",
     ...
]

shadow-exclude = [
     "! name~=''",
     ...
]
```

#### Percetages, direct values are sometimes off a few pixels.
This drove me crazy as well, but then I realized it might be the terminal: it can't be half character wide, or half line tall, so it will do some rounding. Percentages are fine, but if you really want to align multiple Conky Terminals pixel perfectly, you might need to tweak direct pixel position, based on the console font you are using.

What was that animated Music Player and Terminal again?
-------------------------------------------------------
That was my [i3-scratchpad], give it a try if you like it. Or the [i3-autoname], since I am already advertising stuff.

That Wallpaper of yours...
--------------------------
It is [Liang-Xing's Battle Angel Alita](https://www.deviantart.com/liang-xing/art/Battle-Angel-Alita-790254355), thank you for asking.


License
-------
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>


[i3wm]: https://i3wm.org/
[Bash]: https://www.gnu.org/software/bash/
[URxvt]: http://software.schmorp.de/pkg/rxvt-unicode.html
[Conky]: https://github.com/brndnmtthws/conky
[i3-scratchpad]: https://gitlab.com/aquator/i3-scratchpad
[i3-autoname]: https://gitlab.com/aquator/i3-autoname
